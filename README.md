# hacks

Test project to try out gitlab.

Some inline math $`dy = 6x^2\,dx`$ here, and a display equation:

```math
f(x) = \frac{d}{dx}\int_0^x f(x')\,dx'
```

Standard Rmd notation for inline $x=x+1$ and display
$$ x = x+1 $$
math?
